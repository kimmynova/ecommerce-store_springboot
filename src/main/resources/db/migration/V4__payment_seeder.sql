INSERT INTO payment_type (id, value,created_at, updated_at)
VALUES (1, 'Credit Card', current_timestamp,current_timestamp),
       (2, 'Paypal', current_timestamp,current_timestamp),
       (3,'Debit Card',current_timestamp,current_timestamp);

INSERT INTO order_status (id, status, updated_at,created_at ,created_by, updated_by)
VALUES (1,'Processing',current_timestamp,current_timestamp,1,null),
       (2,'Delivered',current_timestamp,current_timestamp,1,null);

INSERT INTO shipping_method ( id,name, price,created_at, updated_at, created_by,updated_by)
VALUES (1,'Cargo Express',10.00,current_timestamp,null,1,null),
       (2,'Swift Shipping Solutions',20.00,current_timestamp,null,1,null),
       (3,'Freight Masters',30.00,current_timestamp,null,1,null);

INSERT INTO shop_order (id,user_id,order_date, shipping_method,order_total, order_status, shipping_address,created_at, updated_at, created_by, updated_by)
VALUES (1,1,current_timestamp,1,100.00,1,'Alin Cargo Express Cambodia',current_timestamp,CURRENT_TIMESTAMP,1,1);

INSERT INTO user_payment_method (id,user_id,payment_type_id, provider, account_number, expiry_date, payment_method_id ,is_default, created_at,updated_at, deleted_at,
                                 created_by, updated_by)
VALUES (1,1,1,'MASTER_CARD','378282246310005','2/26',1,true,current_timestamp,null,null,1,null),
       (2,1,2,'VISA','478282246310005','7/26',1,false,current_timestamp,null,null,1,null),
       (3,1,3,'UNION_PAY','678282246310005','5/26',1,false,current_timestamp,null,null,1,null);

INSERT INTO order_line (id,order_id,product_item_id,qty,price,created_at,updated_at,deleted_at, created_by, updated_by)
VALUES (1,1,1,1,100.00,CURRENT_TIMESTAMP,null,null,1,null);

