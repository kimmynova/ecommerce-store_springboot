package com.example.demo.db.entity;

import com.example.demo.base.BaseEntity;
import jakarta.persistence.*;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import java.math.BigDecimal;
import java.util.List;
import java.util.Set;

/**
 * @author Sattya
 * create at 1/27/2024 1:47 PM
 */
@Entity
@Table(name = "products",indexes = {
        @Index(name = "idx_product_uuid",columnList = "uuid",unique = true),
        @Index(name = "idx_product_name",columnList = "name")
})
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ProductEntity extends BaseEntity{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(unique = true,nullable = false)
    private String uuid;

    @Size(max = 150)
    @Column(name = "name",nullable = false)
    private String name;

    @Column(columnDefinition = "TEXT")
    private String description;

    @Column(name = "image_url")
    private String image;

    @ManyToOne
    @JoinColumn(name = "category_id")
    private CategoryEntity category;

    @OneToMany(mappedBy = "product")
    List<ProductItemEntity> productItems;

    @ManyToMany(mappedBy = "products")
    private List<UserReviewEntity> userReviews;

}
