package com.example.demo.db.entity;

import com.example.demo.base.BaseEntity;
import jakarta.persistence.*;
import jakarta.validation.constraints.Positive;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

/**
 * @author Sattya
 * create at 2/7/2024 3:53 PM
 */

@Entity
@Table(name = "user_review",indexes = {
        @Index(name = "idx_user_review_user_id", columnList = "user_id")
})
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserReviewEntity extends BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private UserEntity user;

   @ManyToMany
    @JoinTable(
            name = "user_review_products",
            joinColumns = @JoinColumn(
                    name = "user_reviews_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(
                    name = "products_id", referencedColumnName = "id"))
    private List<ProductEntity>products;

    @Column(name = "rating_value")
    @Positive
    private Double ratingValue;

    @Size(max = 200)
    @Column(name = "comment")
    private String comment;
    private String image;
}
