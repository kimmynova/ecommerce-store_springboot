package com.example.demo.db.entity;

import com.example.demo.base.BaseEntity;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

/**
 * @author Sattya
 * create at 2/7/2024 1:41 AM
 */
@Entity
@Table(name = "variations",indexes = {
        @Index(name = "idx_variation_name",columnList = "name")
})
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class VariationEntity extends BaseEntity {
    @Id
    @GeneratedValue(strategy = jakarta.persistence.GenerationType.IDENTITY)
    private Long id;
    @Column(nullable = false)
    private String name;

    @ManyToOne
    private CategoryEntity category;

    @OneToMany(fetch = FetchType.EAGER,mappedBy = "variation")
    private List<VariationOptionEntity> options;

}
