package com.example.demo.db.repository;

import com.example.demo.db.entity.PaymentTypeEntity;
import com.example.demo.model.projection.UserPayment.PaymentTypeEntityInfo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;


public interface PaymentTypeRepository extends JpaRepository<PaymentTypeEntity, Long> {
    @Query("SELECT p FROM PaymentTypeEntity p WHERE (:query = 'all' OR LOWER(p.value) LIKE CONCAT('%', LOWER(:query), '%')) AND p.deletedAt IS NULL")
    Page<PaymentTypeEntityInfo> findByValueAndDeletedAtNull(@Param("query") String value, Pageable pageable);

    @Query("select (count(p) > 0) from PaymentTypeEntity p where upper(p.value) = upper(?1)")
    boolean existValue(String value);

    @Query("select p from PaymentTypeEntity p where p.value = :value and p.deletedAt is not null")
    PaymentTypeEntity findByValueAndDeletedAtNotNull(@Param("value") String value);

    @Query("select (count(p) > 0) from PaymentTypeEntity p where p.id = :id and p.deletedAt is not null ")
    boolean existsByIdAndDeletedAt(@Param("id") Long id);
}