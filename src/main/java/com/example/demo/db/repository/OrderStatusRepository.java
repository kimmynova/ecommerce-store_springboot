package com.example.demo.db.repository;

import com.example.demo.db.entity.OrderStatusEntity;
import com.example.demo.model.projection.OrderStatus.OrderStatusEntityInfo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface OrderStatusRepository extends JpaRepository<OrderStatusEntity, Long> {
    @Query("select o from OrderStatusEntity o where ( :query='all' or o.status = :query) and o.deletedAt is null order by o.id desc ")
    Page<OrderStatusEntityInfo> searchStatus(String query, Pageable pageable);

    @Query("select (count(o) > 0) from OrderStatusEntity o where o.status = :status and o.deletedAt is null")
    boolean existsByStatus(@Param("status") String status);

    @Query("select (count(o) > 0) from OrderStatusEntity o where o.deletedAt is not null")
    boolean existsByDeleted();


}