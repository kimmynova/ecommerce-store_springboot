package com.example.demo.db.repository;

import com.example.demo.db.entity.VariationEntity;
import com.example.demo.db.entity.VariationOptionEntity;
import com.example.demo.model.projection.variation.VariationEntityInfo;
import com.example.demo.model.projection.variationOption.VariationOptionEntityInfo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

/**
 * @author Sattya
 * create at 2/11/2024 12:53 AM
 */
public interface VariationOptionRepository extends JpaRepository<VariationOptionEntity, Long>{
    @Query("select v from VariationOptionEntity v where (:value='all'or v.value = :value)and v.deletedAt is null")
    Page<VariationOptionEntityInfo> findByValue(@Param("value") String value, Pageable pageable);

    Page<VariationOptionEntityInfo> findByIdEqualsAndDeletedAtIsNull(Long id, Pageable pageable);

    @Query("select (count(v) > 0) from VariationOptionEntity v where v.value = :value and v.deletedAt is null")
    boolean existsByValue(@Param("value") String value);
    boolean existsByValueAndIdNot(String value, Long id );

}

