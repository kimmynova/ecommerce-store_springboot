package com.example.demo.db.repository;

import com.example.demo.db.entity.VariationEntity;
import com.example.demo.model.projection.variation.VariationEntityInfo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

/**
 * @author Sattya
 * create at 2/11/2024 12:43 AM
 */
public interface VariationRepository extends JpaRepository<VariationEntity, Long>{
    @Query("select v from VariationEntity v where (:name='all'or v.name = :name)and v.deletedAt is null")
    Page<VariationEntityInfo> findByName(@Param("name") String name, Pageable pageable);

    Page<VariationEntityInfo> findByIdEqualsAndDeletedAtIsNull(Long id, Pageable pageable);

    boolean existsByName(String name);

    boolean existsByNameAndIdNot(String name, Long id );

    boolean existsByIdAndDeletedAtNotNull(Long id);
}
