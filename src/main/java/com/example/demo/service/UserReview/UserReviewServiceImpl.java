package com.example.demo.service.UserReview;

import com.example.demo.base.BaseListingRQ;
import com.example.demo.base.BaseService;
import com.example.demo.base.StructureRS;
import com.example.demo.constant.MessageConstant;
import com.example.demo.db.entity.ProductEntity;
import com.example.demo.db.entity.UserEntity;
import com.example.demo.db.entity.UserReviewEntity;
import com.example.demo.db.repository.ProductRepository;
import com.example.demo.db.repository.UserRepository;
import com.example.demo.db.repository.UserReviewRepository;
import com.example.demo.exception.httpstatus.BadRequestException;
import com.example.demo.exception.httpstatus.NotFoundException;
import com.example.demo.model.projection.UserReview.UserReviewEntityInfo;
import com.example.demo.model.request.UserReviewRQ.UpdateUserReviewRQ;
import com.example.demo.model.request.UserReviewRQ.UserReviewRQ;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

@Service
@AllArgsConstructor
public class UserReviewServiceImpl extends BaseService implements UserReviewService {

    private final UserRepository userRepository;
    private final UserReviewRepository userReviewRepository;
    private final ProductRepository productRepository;

    @Override
    public StructureRS getAllUserReview(Double rating_value,BaseListingRQ request) {

    Page<UserReviewEntityInfo>userReviewEntityInfos=userReviewRepository.findByRatingValueAndDeletedAtNull(rating_value,request.getPageable());
        return response(userReviewEntityInfos.getContent(),userReviewEntityInfos);
    }

    @Override
    public StructureRS createUserreview(UserReviewRQ request) {
        UserEntity user=userRepository.findByUsernameAndIsDeletedFalseAndStatusTrue(SecurityContextHolder.getContext().getAuthentication().getName()).orElseThrow(()->new BadRequestException(MessageConstant.AUTH.INVALID_TOKEN));
        Long producdID=request.getProduct_Id().stream().findFirst().orElseThrow(()->new NotFoundException( MessageConstant.User_Reviews.ORDERIDPRODUCTIDNOTFOUND));
        ProductEntity productEntity=productRepository.findById(producdID).orElseThrow(()->new NotFoundException( MessageConstant.User_Reviews.ORDERIDPRODUCTIDNOTFOUND));
        List<ProductEntity> products = new ArrayList<>();
        products.add(productEntity);

        UserReviewEntity userReview=new UserReviewEntity();
        userReview.setUser(user);
        userReview.setRatingValue(request.getRatingValue());
        userReview.setComment(request.getComment());
        userReview.setImage(request.getImages());
        userReview.setProducts(products);
        userReviewRepository.save(userReview);
        return response();
    }

    @Override
    public void updateUserReview(Long id, UpdateUserReviewRQ request) {
        UserReviewEntity userReview=userReviewRepository.findById(id).orElseThrow(()-> new NotFoundException(MessageConstant.User_Reviews.USERREVIEWCANNOTFINDID));
        userReview.setRatingValue(request.getRatingValue());
        userReview.setComment(request.getComment());
        userReview.setImage(request.getImages());
        userReviewRepository.save(userReview);
    }
    @Override
    public void deleteUserReview(Long id) {
        UserReviewEntity userReview=userReviewRepository.findById(id).orElseThrow(()-> new NotFoundException(MessageConstant.User_Reviews.USERREVIEWCANNOTFINDID));
        Boolean isDeleted=userReviewRepository.existsByIdAndDeletedAtNotNull(id);

        if (isDeleted){
            throw new NotFoundException(MessageConstant.User_Reviews.ALREADYDELETEUSERREVIEWS);
        }else {
            userReview.setDeletedAt(Instant.now());
            userReviewRepository.save(userReview);
        }
    }
}
