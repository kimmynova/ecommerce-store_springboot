package com.example.demo.service.ShoppingCart;

import com.example.demo.base.BaseListingRQ;
import com.example.demo.base.StructureRS;
import com.example.demo.model.request.ShoppingCart.ShoppingCartRQ;
import com.example.demo.model.request.ShoppingCart.UpdateShoppingCartRQ;
import com.example.demo.model.request.ShoppingCart.UpdateStatusShoppingCartRQ;
import com.example.demo.security.UserPrincipal;

public interface ShoppingCartService {
    StructureRS getShoppingCart(UserPrincipal userPrincipal, BaseListingRQ request);
    void createShoppingCart(ShoppingCartRQ request);
    void updateShoppingCart(UserPrincipal userPrincipal,UpdateShoppingCartRQ request);
    void updateShoppingCartStatus(UserPrincipal userPrincipal, UpdateStatusShoppingCartRQ[] request);
}
