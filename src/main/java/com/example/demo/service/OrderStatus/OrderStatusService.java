package com.example.demo.service.OrderStatus;

import com.example.demo.base.BaseListingRQ;
import com.example.demo.base.StructureRS;
import com.example.demo.model.request.OrderStatus.OrderStatusRQ;
import com.example.demo.model.request.OrderStatus.UpdateOrderStatusRQ;

public interface OrderStatusService {
    StructureRS getOrderStatus(BaseListingRQ request);
    void createOrderStatus(OrderStatusRQ request);
    void updateOrderStatus(Long id, UpdateOrderStatusRQ request);
    void deleteOrderStatus(Long id);
}
