package com.example.demo.service.Orderline;

import com.example.demo.base.BaseListingRQ;
import com.example.demo.base.StructureRS;
import com.example.demo.model.request.Orderline.OrderlineRQ;
import com.example.demo.model.request.Orderline.ProductItemRQ;
import com.example.demo.model.request.Orderline.UpdateOrderlineRQ;
import com.example.demo.model.request.Orderline.UpdateStatusOrderlineRQ;

import java.util.List;

public interface OrderLineService {
    /**
     *get all Order Items
     */
    StructureRS getALlOrderline(Long order_id,BaseListingRQ request);

    /**
     *
     * @param request for body orderline
     */
    void createOrderline(OrderlineRQ request);
    /**
     *
     * @param id for delete orderline
     */
    void deleteOrderLine(Long id);

}
