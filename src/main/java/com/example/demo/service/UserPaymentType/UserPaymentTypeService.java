package com.example.demo.service.UserPaymentType;

import com.example.demo.base.BaseListingRQ;
import com.example.demo.base.StructureRS;
import com.example.demo.model.request.UserPaymentRQ.UserPaymentTypeRQ;

public interface UserPaymentTypeService {
    /**
     *Get All Payment Types and Searching
     */
    StructureRS getPaymentType(BaseListingRQ request);

    /**
     *
     * @param request is request body of payment type
     */
    void createPaymentType(UserPaymentTypeRQ request);

    /**
     *
     * @param id for payment type when it needs to update
     * @param request
     */
    void updatePaymentType(Long id, UserPaymentTypeRQ request);

    /**
     *
     * @param id for payment type when it needs to delete
     */
    void deletePaymentType(Long id);
}
