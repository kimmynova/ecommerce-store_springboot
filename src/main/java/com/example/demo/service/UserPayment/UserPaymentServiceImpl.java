package com.example.demo.service.UserPayment;

import com.example.demo.base.BaseListingRQ;
import com.example.demo.base.BaseService;
import com.example.demo.base.StructureRS;
import com.example.demo.constant.MessageConstant;
import com.example.demo.db.entity.PaymentTypeEntity;
import com.example.demo.db.entity.UserEntity;
import com.example.demo.db.entity.UserPaymentMethodEntity;
import com.example.demo.db.repository.PaymentTypeRepository;
import com.example.demo.db.repository.ShopOrderRepository;
import com.example.demo.db.repository.UserPaymentMethodRepository;
import com.example.demo.db.repository.UserRepository;
import com.example.demo.db.status.Provider;
import com.example.demo.exception.httpstatus.BadRequestException;
import com.example.demo.exception.httpstatus.InternalServerError;
import com.example.demo.exception.httpstatus.NotFoundException;
import com.example.demo.model.projection.UserPayment.UserPaymentMethodInfo;
import com.example.demo.model.request.UserPaymentRQ.UserPaymentRQ;
import com.example.demo.security.UserPrincipal;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.List;

@Service
@RequiredArgsConstructor
public class UserPaymentServiceImpl extends BaseService implements UserPaymentService {
    private final UserRepository userRepository;
    private final UserPaymentMethodRepository userPaymentMethodRepository;
    private final PaymentTypeRepository paymentTypeRepository;
    private final ShopOrderRepository shopOrderRepository;

    @Override
    public StructureRS getPayment(BaseListingRQ request, Provider provider) {
        Page<UserPaymentMethodInfo> userPaymentMethodEntity = userPaymentMethodRepository.findUserPayment(provider, request.getPageable(request.getSort(),request.getOrder()));
        return response(userPaymentMethodEntity.getContent(), userPaymentMethodEntity);
    }

    @Override
    @Transactional
    public void Create(UserPaymentRQ request,UserPrincipal userPrincipal) {

        UserEntity userEntity = userRepository.findByUsernameAndIsDeletedFalseAndStatusTrue(SecurityContextHolder.getContext().getAuthentication().getName()).orElseThrow(() ->
                new BadRequestException(MessageConstant.AUTH.INVALID_TOKEN));
        PaymentTypeEntity paymentType = paymentTypeRepository.findById(request.getPaymentTypeId())
                .orElseThrow(() -> new NotFoundException(MessageConstant.Payment_Method.PAYMENTTYPENOTFOUND));

        boolean isExistAccount = userPaymentMethodRepository.existAccountNumber(request.getAccount_number());
        UserPaymentMethodEntity userPayment = new UserPaymentMethodEntity();

        if (isExistAccount) {
            throw new InternalServerError(MessageConstant.Payment_Method.ACCOUNTALREADYEXIST);
        } else {
            UserPaymentMethodEntity currentDefault = userPaymentMethodRepository.findByProviderAndIsDefaultTrue(userPrincipal.getId());
            if (currentDefault != null) {
                currentDefault.setIsDefault(false);
                userPaymentMethodRepository.save(currentDefault);
            }
            userPayment.setUser(userEntity);
            userPayment.setPaymentType(paymentType);
            userPayment.setAccountNumber(request.getAccount_number());
            userPayment.setProvider(request.getProvider());
            userPayment.setExpiryDate(request.getExpiry_date());
            userPayment.setIsDefault(true);

//            Long payment_method_id = request.getPayment_method_id().stream().findFirst().orElseThrow(() -> new InternalServerError(MessageConstant.Payment_Method.PAYMENTMETHODIDALREADYEXIST));
//            Optional<ShopOrderEntity> paymentMethodID = shopOrderRepository.findById(payment_method_id);
//            userPayment.setShopOrder(paymentMethodID.orElseThrow(() -> new NotFoundException(MessageConstant.Payment_Method.PAYMENTMETHODIDNOTFOUND)));
            userPayment.setShopOrder(null);

            userPaymentMethodRepository.save(userPayment);
        }

    }

    @Override
    @Transactional
    public void Update(Long id, UserPaymentRQ request) {
        UserEntity userEntity = userRepository.findByUsernameAndIsDeletedFalseAndStatusTrue(SecurityContextHolder.getContext().getAuthentication().getName()).orElseThrow(() ->
                new BadRequestException(MessageConstant.AUTH.INVALID_TOKEN));
        PaymentTypeEntity paymentType = paymentTypeRepository.findById(request.getPaymentTypeId())
                .orElseThrow(() -> new NotFoundException(MessageConstant.Payment_Method.PAYMENTTYPENOTFOUND));
        UserPaymentMethodEntity userPaymentMethod = userPaymentMethodRepository.findById(id).orElseThrow(() -> new NotFoundException(MessageConstant.Payment_Method.USERPAYMENTMETHODIDNOTFOUND));

        userPaymentMethod.setUser(userEntity);
        userPaymentMethod.setProvider(request.getProvider());
        userPaymentMethod.setPaymentType(paymentType);
        userPaymentMethod.setAccountNumber(request.getAccount_number());
        userPaymentMethod.setExpiryDate(request.getExpiry_date());

        userPaymentMethodRepository.save(userPaymentMethod);

    }

    @Override
    @Transactional
    public void Delete(Long id) {
        UserPaymentMethodEntity userPaymentMethod_Id = userPaymentMethodRepository.findById(id)
                .orElseThrow(() -> new NotFoundException(MessageConstant.Payment_Method.USERPAYMENTMETHODIDNOTFOUND));
        boolean alreadyDelete = userPaymentMethodRepository.alreadyDelete(id);
        if (alreadyDelete){
            throw new InternalServerError(MessageConstant.Payment_Method.ALREADYDELETED);
        }else {
            userPaymentMethod_Id.setDeletedAt(Instant.now());
            userPaymentMethodRepository.save(userPaymentMethod_Id);
        }

    }
    @Override
    public StructureRS getPaymentToken(UserPrincipal principal) {
        List<UserPaymentMethodInfo> optionalUserPaymentMethodInfo = userPaymentMethodRepository.findByUser_IdAndIsDefaultNull(principal.getId());
        if (optionalUserPaymentMethodInfo == null)
            throw new BadRequestException(MessageConstant.Payment_Method.PAYMENTTYPENOTFOUND);
        return response(optionalUserPaymentMethodInfo);
    }

    @Override
    public void updateStatusDefault(Long id) {

        UserPaymentMethodEntity currentDefaultPayment = userPaymentMethodRepository.findById(id).orElseThrow(() -> new NotFoundException(MessageConstant.Payment_Method.PAYMENTTYPENOTFOUND));
        List<UserPaymentMethodEntity> userPaymentMethodEntities = userPaymentMethodRepository.findAllByUserId(currentDefaultPayment.getUser().getId());

        userPaymentMethodEntities.forEach(u -> u.setIsDefault(Boolean.FALSE));

        userPaymentMethodRepository.saveAll(userPaymentMethodEntities);
        currentDefaultPayment.setIsDefault(Boolean.TRUE);
        userPaymentMethodRepository.save(currentDefaultPayment);
    }


}
