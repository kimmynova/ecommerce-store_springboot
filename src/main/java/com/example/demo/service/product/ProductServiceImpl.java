package com.example.demo.service.product;

import com.example.demo.base.BaseListingRQ;
import com.example.demo.base.BaseService;
import com.example.demo.base.StructureRS;
import com.example.demo.constant.MessageConstant;
import com.example.demo.db.entity.ProductEntity;
import com.example.demo.db.entity.ProductItemEntity;
import com.example.demo.db.entity.UserReviewEntity;
import com.example.demo.db.repository.CategoryRepository;
import com.example.demo.db.repository.ProductItemRepository;
import com.example.demo.db.repository.ProductRepository;
import com.example.demo.db.repository.UserReviewRepository;
import com.example.demo.exception.httpstatus.InternalServerError;
import com.example.demo.exception.httpstatus.NotFoundException;
import com.example.demo.mapper.ProductEntityMapper;
import com.example.demo.model.projection.product.ProductEntityInfo;
import com.example.demo.model.projection.product.ProductUserReviewEntityInfo;
import com.example.demo.model.request.product.CreateProductRQ;
import com.example.demo.model.request.product.UpdateProductRQ;
import com.example.demo.model.request.product.UpdateStatusProductRQ;
import com.example.demo.model.response.product.ProductEntityDto;
import com.example.demo.model.response.productItems.ProductItemsRS;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author Sattya
 * create at 2/10/2024 8:23 PM
 */
@Service
@RequiredArgsConstructor
@Slf4j
public class ProductServiceImpl extends BaseService implements ProductService{
    private final ProductRepository productRepository;
    private final CategoryRepository categoryRepository;
    private final ProductEntityMapper productEntityMapper;
    private final ProductItemRepository productItemRepository;
    @Override
    public StructureRS getAll(BaseListingRQ request) {
        Page<ProductEntityInfo> productEntityInfoPage = productRepository.findByQuery(request.getQuery(),request.getPageable(request.getSort(),request.getOrder()));
        return response(productEntityInfoPage.getContent(),productEntityInfoPage);
    }

    @Override
    public StructureRS getUseReviewByProduct(String uuid,BaseListingRQ request) {
        Page<ProductUserReviewEntityInfo> productEntityInfoPage = productRepository.findByUuidAndDeletedAtNull(uuid,request.getPageable(request.getSort(),request.getOrder()));
        return response(productEntityInfoPage.getContent(),productEntityInfoPage);
    }

    @Override
    public StructureRS findByUuid(String uuid, BaseListingRQ request) {
        // Check if the product with the given UUID exists
        if (!productRepository.existsByUuidAndDeletedAtIsNull(uuid)){
            throw new NotFoundException(MessageConstant.PRODUCT.PRODUCT_NOT_FOUND);
        }

        // Retrieve product details DTO by UUID
        ProductEntityDto productEntityDto = productEntityMapper.toProductEntityDto(productRepository.findByUuid(uuid));

        // Retrieve product item entities associated with the product
        List<ProductItemEntity> productItemEntities = productItemRepository.findAllByProductUuid(uuid);

        // Map product item entities to DTOs
        List<ProductItemsRS> productItemsRSList = productItemEntities.stream()
                .map(productItemEntity -> {
                    ProductItemsRS productRs = new ProductItemsRS();
                    productRs.setProduct_id(productItemEntity.getProduct().getId());
                    productRs.setPrice(productItemEntity.getPrice());
                    return productRs;
                })
                .collect(Collectors.toList());

        // Construct response
        Map<String, Object> responseData = new HashMap<>();
        responseData.put("product", productEntityDto);
        responseData.put("productItems", productItemsRSList);

        return response(responseData);
    }

    @Transactional
    @Override
    public StructureRS createNew(CreateProductRQ request) {
        try{
            if (productRepository.existsByName(request.getName())){
                throw new InternalServerError(MessageConstant.PRODUCT.PRODUCT_ALREADY_EXISTS);
            }
            ProductEntity productEntity = productEntityMapper.toEntity(request);
            productEntity.setUuid(UUID.randomUUID().toString());
            if (request.getCategoryId() != null){
                productEntity.setCategory(categoryRepository.findById(Long.valueOf(request.getCategoryId())).orElseThrow(() -> new NotFoundException(MessageConstant.CATEGORY.CATEGORY_NOT_FOUND)));
                productRepository.save(productEntity);
            }else {
                throw new NotFoundException(MessageConstant.CATEGORY.CATEGORY_NOT_FOUND);
            }
        }catch (Exception e){
            e.printStackTrace();
            throw new InternalServerError(e.getMessage());
        }
        return response(MessageConstant.PRODUCT.PRODUCT_CREATED_SUCCESSFULLY);
    }
    @Transactional
    @Override
    public void updateByUuid(String uuid, UpdateProductRQ request) {
    ProductEntity productEntity = getProductByUuid(uuid);
    checkProductExistsByName(request.getName());
    partialUpdateProduct(productEntity,request);
    saveProduct(productEntity);
    }
    private ProductEntity getProductByUuid(String uuid){
        if (!productRepository.existsByUuidAndDeletedAtIsNull(uuid)){
            throw new NotFoundException(MessageConstant.PRODUCT.PRODUCT_NOT_FOUND);
        }
        return productRepository.findByUuid(uuid);
    }
    private void checkProductExistsByName(String name){
        if (productRepository.existsByName(name)){
            throw new InternalServerError(MessageConstant.PRODUCT.PRODUCT_ALREADY_EXISTS);
        }
    }
    private void partialUpdateProduct(ProductEntity productEntity,UpdateProductRQ request){
        productEntityMapper.partialUpdate(request,productEntity);
        if (request.getCategoryId() != null){
            productEntity.setCategory(categoryRepository.findById(Long.valueOf(request.getCategoryId())).orElseThrow(() -> new NotFoundException(MessageConstant.CATEGORY.CATEGORY_NOT_FOUND)));
        }
    }

    private void saveProduct(ProductEntity productEntity){
        productRepository.save(productEntity);
    }
    @Transactional
    @Override
    public StructureRS updateStatusProducts(String uuid,UpdateStatusProductRQ request) {
        boolean isFound = productRepository.existsByUuid(uuid);
        if (!isFound){
            throw new NotFoundException(MessageConstant.PRODUCT.PRODUCT_NOT_FOUND);
        }
        Instant deletedAt = request.getStatus() ? null : Instant.now();
        productRepository.findByUuid(uuid).setDeletedAt(deletedAt);

        String message = request.getStatus() ? MessageConstant.PRODUCT.PRODUCT_RESTORED_SUCCESSFULLY : MessageConstant.PRODUCT.PRODUCT_DELETED_SUCCESSFULLY;
        return response(message);
    }
}
