package com.example.demo.mapper;

import com.example.demo.db.entity.CategoryEntity;
import com.example.demo.model.request.category.CategoryDto;
import com.example.demo.model.request.category.UpdateCategoryRQ;
import com.example.demo.model.response.category.CategoryEntityDto;
import org.mapstruct.*;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = MappingConstants.ComponentModel.SPRING)
public interface CategoryEntityMapper {
    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    void partialUpdate(UpdateCategoryRQ updateCategoryRQ, @MappingTarget CategoryEntity categoryEntity);

//    @InheritInverseConfiguration(name = "toEntity")
    @Mapping(source = "parent.id",target = "parentId")
    @Mapping(source = "parent.uuid",target = "parentUuid")
    @Mapping(source = "parent.name",target = "parentName")
    @Mapping(source = "parent.description",target = "parentDescription")
    CategoryEntityDto toDto1(CategoryEntity categoryEntity);

    CategoryEntity toEntity(CategoryDto categoryDto);
}