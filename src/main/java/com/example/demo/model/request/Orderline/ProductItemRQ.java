package com.example.demo.model.request.Orderline;

import jakarta.validation.Valid;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;
import lombok.Getter;
import lombok.Setter;

import java.util.HashSet;
import java.util.Set;
@Getter
@Setter
public class ProductItemRQ {

    @NotEmpty(message = "Please Provide Product Item Id")
    private Set<Long> product_item_id;
    @NotNull(message = "Please Provide Quantity")
    private Integer qty;
}
