package com.example.demo.model.request.user;

import com.example.demo.exception.anotation.FieldsValueMatch;
import jakarta.validation.constraints.*;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
@Builder
@FieldsValueMatch(field = "password",fieldMatch = "confirmPassword")
public class UpdateUserRQ {
    @NotBlank
    String username;
    @NotBlank
    String password;

    @NotBlank
    String confirmPassword;

    @NotBlank
    @Email
    String email;
    @NotBlank
    @Size(max = 100)
    String name;
    @Size(max = 200)
    @NotBlank
    String bio;

    String avatar;

    @Size(max = 200)
    String address;

    @Size(min = 8, max = 50)
    String phone;

    @NotNull(message = "At least one role ID must be provided.")
    @Size(max = 1, message = "At least one role ID must be provided.")
    private Set<Long> roleId;
}
