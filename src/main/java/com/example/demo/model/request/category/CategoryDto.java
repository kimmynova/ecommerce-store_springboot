package com.example.demo.model.request.category;

import jakarta.validation.constraints.NotBlank;

import java.io.Serializable;

/**
 * DTO for {@link com.example.demo.db.entity.CategoryEntity}
 */
public record CategoryDto(
        @NotBlank(message = "Name is required")
        String name,
        @NotBlank(message = "Description is required")
        String description,
        Long parentId){

}