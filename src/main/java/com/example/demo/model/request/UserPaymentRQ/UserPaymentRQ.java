    package com.example.demo.model.request.UserPaymentRQ;

    import com.example.demo.db.status.Provider;
    import jakarta.persistence.EnumType;
    import jakarta.persistence.Enumerated;

    import jakarta.validation.Valid;
    import jakarta.validation.constraints.NotBlank;
    import jakarta.validation.constraints.NotNull;
    import jakarta.validation.constraints.Positive;
    import jakarta.validation.constraints.Size;

    import lombok.Data;

    import java.util.Set;


    @Data
    public class UserPaymentRQ {

      @NotNull(message = "Please provide a Payment Type")
      private Long paymentTypeId;
      @NotNull(message = "Please provide a provider")
      @Enumerated(EnumType.STRING)
      private Provider provider;
      @NotBlank(message = "Please make sure to enter your account number")
      private String account_number;

      @NotBlank(message = "Please make sure to enter the expiry date")
      private String expiry_date;

    Set<@Positive Long> payment_method_id;

    }
