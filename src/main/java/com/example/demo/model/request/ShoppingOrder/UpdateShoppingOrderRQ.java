package com.example.demo.model.request.ShoppingOrder;

import jakarta.validation.constraints.NotEmpty;
import lombok.Getter;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
public class UpdateShoppingOrderRQ {
    @NotEmpty(message = "Please provide shipping")
    Long shippingMethod;
    @NotEmpty(message = "Please provide shipping address")
    private String shippingAddress;
}
