package com.example.demo.model.request.ShoppingOrder;

import lombok.Getter;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
public class UpdateShoppingOrderStatus {
    Long orderstatus;
}
