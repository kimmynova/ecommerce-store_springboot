package com.example.demo.model.request.ShoppingOrder;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.Set;

@Getter
@Setter
public class ShoppingOrderRQ {
    @NotNull(message = "Please provide shipping")
    Long shippingMethod;
    @NotNull(message = "Please provide pay method")
    Long user_payment_method;
    @NotEmpty(message = "Please provide shipping address")
    private String shippingAddress;
    Set<ProductitemsRQ> orderlines;
}
