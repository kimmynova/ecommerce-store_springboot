package com.example.demo.model.request.ShoppingOrder;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class CartItemRQ {
    private Long product_item_id;
    private Integer qty;
    private BigDecimal price;

}
