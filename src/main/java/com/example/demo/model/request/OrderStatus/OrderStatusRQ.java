package com.example.demo.model.request.OrderStatus;

import jakarta.validation.constraints.NotEmpty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OrderStatusRQ {
    @NotEmpty(message = "Please provide name of status")
    private String status;
}
