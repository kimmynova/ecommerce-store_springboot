package com.example.demo.model.request.UserReviewRQ;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;
import lombok.Getter;
import lombok.Setter;

import java.util.Set;
@Getter
@Setter
public class UpdateUserReviewRQ {

    @NotNull(message = "need rate")
    private double ratingValue;

    @NotEmpty(message = "Please Write Some Comment on Product")
    private String comment;

    private String images;

}
