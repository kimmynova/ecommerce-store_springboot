package com.example.demo.model.request.ShoppingCart;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Range;

import java.util.Set;

@Getter
@Setter
public class UpdateShoppingCartRQ {
    @Range(min = 1,message = "Please provide number of quantity")
    @NotNull(message ="Please provide number of quantity")
    private Integer qty;
    @NotNull(message ="Please provide product item")
    private Long product_item_id;
}
