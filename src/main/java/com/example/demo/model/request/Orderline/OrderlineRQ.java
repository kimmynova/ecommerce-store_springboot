package com.example.demo.model.request.Orderline;

import jakarta.validation.Valid;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;
import lombok.Getter;
import lombok.Setter;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Getter
@Setter
public class OrderlineRQ {

    @NotEmpty(message = "Please Provide Your Order Id")
    private Set<@Positive Long> order_id;

    private List<@Valid ProductItemRQ>orderline;



}
