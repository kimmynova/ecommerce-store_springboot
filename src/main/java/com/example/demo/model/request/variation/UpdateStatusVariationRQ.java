package com.example.demo.model.request.variation;

import jakarta.validation.constraints.NotNull;
import lombok.Data;

@Data
public class UpdateStatusVariationRQ {
    @NotNull(message = "status is required")
    private Boolean status;
}
