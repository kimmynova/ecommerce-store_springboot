package com.example.demo.model.request.variation;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class CreateVariationOptionRQ {
    @NotBlank(message =  "Value is required")
    private String value;
    @NotNull(message =  "VariationId is required")
    private Long variationId;
}
