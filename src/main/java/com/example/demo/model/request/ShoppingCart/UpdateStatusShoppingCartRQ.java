package com.example.demo.model.request.ShoppingCart;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class  UpdateStatusShoppingCartRQ {
    private Long itemid;
    @NotEmpty(message = "Please provide status")
    @NotNull(message = "Please provide status")
    private Boolean status;

}
