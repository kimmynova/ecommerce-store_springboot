package com.example.demo.model.response.category;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CategoryItemRS {
    private Long id;
    private String name;
    private String uuid;

}
