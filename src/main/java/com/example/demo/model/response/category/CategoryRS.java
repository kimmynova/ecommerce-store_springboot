package com.example.demo.model.response.category;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class CategoryRS {
    private CategoryItemRS category;
    private List<CategoryItemRS> items = new ArrayList<>();
}
