package com.example.demo.model.response.PermissionRS;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PermissionRS {
    private Long id;
    private String name;
    private String module;
    private Integer status;
}

