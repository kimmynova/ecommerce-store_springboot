package com.example.demo.model.response.category;

import java.io.Serializable;

/**
 * DTO for {@link com.example.demo.db.entity.CategoryEntity}
 */
public record CategoryEntityDto(Long id, String uuid, String name, String description, Long parentId, String parentUuid,
                                String parentName, String parentDescription) implements Serializable {
}