package com.example.demo.model.response.userreview;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class ProductReviewRS {
    private List<UserReviewRS>user_review=new ArrayList<>();

}
