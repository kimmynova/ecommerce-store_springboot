package com.example.demo.model.response.userreview;

import lombok.Data;

import java.util.Set;

@Data
public class UserReviewRS {
    private Long id;
    private Set<Long> user_id;
    private Set<Long> productItem;
    private Integer rate;
    private String comment;
}
