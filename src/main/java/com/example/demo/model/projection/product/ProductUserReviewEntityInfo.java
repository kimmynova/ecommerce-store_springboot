package com.example.demo.model.projection.product;

import java.util.List;

/**
 * Projection for {@link com.example.demo.db.entity.ProductEntity}
 */
public interface ProductUserReviewEntityInfo {
    String getUuid();

    String getName();

    String getDescription();

    String getImage();

    List<UserReviewEntityInfo> getUserReviews();

    /**
     * Projection for {@link com.example.demo.db.entity.UserReviewEntity}
     */
    interface UserReviewEntityInfo {
        Long getId();

        Double getRatingValue();

        String getComment();

        String getImage();

        UserEntityInfo getUser();

        /**
         * Projection for {@link com.example.demo.db.entity.UserEntity}
         */
        interface UserEntityInfo {
            String getName();

            String getAvatar();
        }
    }
}