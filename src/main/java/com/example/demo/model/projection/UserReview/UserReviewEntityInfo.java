package com.example.demo.model.projection.UserReview;

import java.time.Instant;
import java.util.List;

/**
 * Projection for {@link com.example.demo.db.entity.UserReviewEntity}
 */
public interface UserReviewEntityInfo {
    Instant getCreatedAt();

    Instant getUpdatedAt();

    Long getId();

    Double getRatingValue();

    String getComment();

    String getImage();

    UserEntityInfo getUser();

    List<ProductEntityInfo> getProducts();

    /**
     * Projection for {@link com.example.demo.db.entity.UserEntity}
     */
    interface UserEntityInfo {
        String getName();

        String getAvatar();
    }

    /**
     * Projection for {@link com.example.demo.db.entity.ProductEntity}
     */
    interface ProductEntityInfo {
        String getUuid();

        String getName();
    }
}