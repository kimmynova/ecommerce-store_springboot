package com.example.demo.model.projection.variationOption;

import java.time.Instant;

/**
 * Projection for {@link com.example.demo.db.entity.VariationOptionEntity}
 */
public interface VariationOptionEntityInfo {
    Long getId();

    String getValue();

    VariationEntityInfo getVariation();

    Instant getDeletedAt();

    Instant getCreatedAt();

    Instant getUpdatedAt();

    /**
     * Projection for {@link com.example.demo.db.entity.VariationEntity}
     */
    interface VariationEntityInfo {
        Long getId();
    }
}