package com.example.demo.model.projection.productItem;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.Date;
import java.util.Set;

/**
 * Projection for {@link com.example.demo.db.entity.ProductItemEntity}
 */
public interface ProductItemEntityInfo {
    Long getId();

    String getCode();

    Integer getQuantity();

    String getImage();

    BigDecimal getPrice();

    ProductEntityInfo getProduct();

    Instant getCreatedAt();
    Instant getUpdatedAt();

    Set<VariationOptionEntityInfo> getVariationOptions();

    /**
     * Projection for {@link com.example.demo.db.entity.ProductEntity}
     */
    interface ProductEntityInfo {
        Long getId();

        String getUuid();

        String getName();

        String getDescription();

        String getImage();

        CategoryEntityInfo getCategory();

        /**
         * Projection for {@link com.example.demo.db.entity.CategoryEntity}
         */
        interface CategoryEntityInfo {
            Long getId();

            String getUuid();

            String getName();

            String getDescription();
        }
    }

    /**
     * Projection for {@link com.example.demo.db.entity.VariationOptionEntity}
     */
    interface VariationOptionEntityInfo {
        Long getId();

        String getValue();

        VariationEntityInfo getVariation();

        /**
         * Projection for {@link com.example.demo.db.entity.VariationEntity}
         */
        interface VariationEntityInfo {
            Long getId();

            String getName();
        }
    }
}