package com.example.demo.model.projection.ShoppingCart;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.List;

/**
 * Projection for {@link com.example.demo.db.entity.ShoppingCartEntity}
 */
public interface ShoppingCartEntityInfo {
    Instant getCreatedAt();

    Instant getUpdatedAt();

    Long getId();

    List<ShoppingCartItemEntityInfo1> getShoppingCartItems();

    /**
     * Projection for {@link com.example.demo.db.entity.ShoppingCartItemEntity}
     */
    interface ShoppingCartItemEntityInfo1 {
        Long getId();

        Integer getQuantity();

        ProductItemEntityInfo getProductItem();

        /**
         * Projection for {@link com.example.demo.db.entity.ProductItemEntity}
         */
        interface ProductItemEntityInfo {
//            Integer getQuantity();

            String getImage();

            BigDecimal getPrice();

            ProductEntityInfo getProduct();

            /**
             * Projection for {@link com.example.demo.db.entity.ProductEntity}
             */
            interface ProductEntityInfo {
                String getName();

                CategoryEntityInfo getCategory();

                /**
                 * Projection for {@link com.example.demo.db.entity.CategoryEntity}
                 */
                interface CategoryEntityInfo {
                    String getName();

//                    List<VariationEntityInfo> getVariations();
//
//                    /**
//                     * Projection for {@link com.example.demo.db.entity.VariationEntity}
//                     */
//                    interface VariationEntityInfo {
//                        List<VariationOptionEntityInfo> getOptions();
//
//                        /**
//                         * Projection for {@link com.example.demo.db.entity.VariationOptionEntity}
//                         */
//                        interface VariationOptionEntityInfo {
//                            String getValue();
//                        }
//                    }
                }
            }
        }
    }
}