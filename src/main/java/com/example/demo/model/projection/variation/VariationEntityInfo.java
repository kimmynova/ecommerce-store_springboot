package com.example.demo.model.projection.variation;

import java.time.Instant;

/**
 * Projection for {@link com.example.demo.db.entity.VariationEntity}
 */
public interface VariationEntityInfo {
    Long getId();

    String getName();

    CategoryEntityInfo getCategory();
//    Instant getCreatedAt();
    Instant getDeletedAt();
    /**
     * Projection for {@link com.example.demo.db.entity.CategoryEntity}
     */
    interface CategoryEntityInfo {
        Long getId();
        String getName();

    }
}