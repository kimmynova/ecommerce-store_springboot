package com.example.demo.model.projection.ShippingMethod;

import java.math.BigDecimal;
import java.time.Instant;

/**
 * Projection for {@link com.example.demo.db.entity.ShippingMethodEntity}
 */
public interface ShippingMethodEntityInfo {
    Instant getCreatedAt();

    Instant getUpdatedAt();

    Long getId();

    String getName();

    BigDecimal getPrice();
}