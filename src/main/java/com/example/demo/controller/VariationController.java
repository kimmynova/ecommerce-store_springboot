package com.example.demo.controller;

import com.example.demo.base.BaseController;
import com.example.demo.base.BaseListingRQ;
import com.example.demo.base.StructureRS;
import com.example.demo.db.entity.VariationEntity;
import com.example.demo.service.variation.VariationService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/v1/variations")
@RequiredArgsConstructor
public class VariationController extends BaseController {
    private final VariationService variationService;

    @GetMapping
    public ResponseEntity<StructureRS> getAllVariation(BaseListingRQ request) {
        return response(variationService.getAllVariations(request));
    }

    @GetMapping("/{id}")
    public ResponseEntity<StructureRS> getVariationOptionById(@PathVariable Long id , BaseListingRQ request) {
        return response(variationService.getVariationById(id ,request));
    }

    @PostMapping()
    public ResponseEntity<StructureRS> createVariation(@RequestBody VariationEntity request){
        return ResponseEntity.ok(variationService.createVariation(request));
    }

    @PutMapping("/{id}")
    public ResponseEntity<StructureRS> updateVariation(@PathVariable Long id, @RequestBody VariationEntity request) {
       variationService.updateVariation(id, request);
        return response(HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<StructureRS> deleteVariation(@PathVariable Long id) {
        variationService.deleteVariation(id);
        return response(HttpStatus.OK);
    }

}
