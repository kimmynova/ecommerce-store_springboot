package com.example.demo.controller;

import com.example.demo.base.BaseController;
import com.example.demo.base.BaseListingRQ;
import com.example.demo.base.StructureRS;
import com.example.demo.model.request.ShoppingCart.ShoppingCartRQ;
import com.example.demo.model.request.ShoppingCart.UpdateShoppingCartRQ;
import com.example.demo.model.request.ShoppingCart.UpdateStatusShoppingCartRQ;
import com.example.demo.security.UserPrincipal;
import com.example.demo.service.ShoppingCart.ShoppingCartService;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/v1/shop_cart")
@AllArgsConstructor
public class ShoppingCartController extends BaseController {
    private final ShoppingCartService shoppingCartService;
    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<StructureRS>getShoppingCart(JwtAuthenticationToken jwtAuthenticationToken, BaseListingRQ request){
        return response(shoppingCartService.getShoppingCart(UserPrincipal.build(jwtAuthenticationToken),request));
    }
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<StructureRS>createShoppingCart(@Valid @RequestBody ShoppingCartRQ request){
        shoppingCartService.createShoppingCart(request);
        return response(HttpStatus.CREATED);
    }
    @PutMapping
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<StructureRS>updateShoppingCart(JwtAuthenticationToken jwtAuthenticationToken,@Valid @RequestBody UpdateShoppingCartRQ request){
        shoppingCartService.updateShoppingCart(UserPrincipal.build(jwtAuthenticationToken),request);
        return response(HttpStatus.OK);
    }
    @PutMapping("/status")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<StructureRS>updateShoppingCartStatus(JwtAuthenticationToken jwtAuthenticationToken, @Valid @RequestBody UpdateStatusShoppingCartRQ[] request){
        shoppingCartService.updateShoppingCartStatus(UserPrincipal.build(jwtAuthenticationToken),request);
        return response(HttpStatus.OK);
    }
}
