package com.example.demo.controller;

import com.example.demo.base.BaseController;
import com.example.demo.base.BaseListingRQ;
import com.example.demo.base.StructureRS;
import com.example.demo.model.request.OrderStatus.OrderStatusRQ;
import com.example.demo.model.request.OrderStatus.UpdateOrderStatusRQ;
import com.example.demo.service.OrderStatus.OrderStatusService;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/v1/order-status")
@AllArgsConstructor
public class OrderStatusController extends BaseController {
    private final OrderStatusService orderStatusService;

    @GetMapping
    public ResponseEntity<StructureRS>getOrderStatus(BaseListingRQ request){
        return response(orderStatusService.getOrderStatus(request));
    }
    @PostMapping
    public ResponseEntity<StructureRS>createOrderStatus(@Valid @RequestBody OrderStatusRQ request){
        orderStatusService.createOrderStatus(request);
        return response(HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<StructureRS>updateOrderStatus(@Valid @PathVariable Long id, @RequestBody UpdateOrderStatusRQ request){
        orderStatusService.updateOrderStatus(id,request);
        return response(HttpStatus.OK);
    }
    @DeleteMapping("/{id}")
    public ResponseEntity<StructureRS>deleteOrderStatus( @PathVariable Long id){
        orderStatusService.deleteOrderStatus(id);
        return response(HttpStatus.OK);
    }
}
