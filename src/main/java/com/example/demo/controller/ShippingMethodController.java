package com.example.demo.controller;

import com.example.demo.base.BaseController;
import com.example.demo.base.BaseListingRQ;
import com.example.demo.base.StructureRS;
import com.example.demo.db.repository.ShippingMethodRepository;
import com.example.demo.model.request.ShippingMethod.ShippingMethodRQ;
import com.example.demo.model.request.ShippingMethod.UpdateShippingMethodRQ;
import com.example.demo.service.ShippingMehtod.ShippingMehtodService;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@AllArgsConstructor
@RestController
@RequestMapping("api/v1/shipping-method")
public class ShippingMethodController extends BaseController {
    private final ShippingMehtodService shippingMehtodService;

    @GetMapping
    public ResponseEntity<StructureRS> getShippingMethod(BaseListingRQ request){
        return response(shippingMehtodService.getShippingMethod(request));
    }
    @PostMapping
    public ResponseEntity<StructureRS>createShippingMethod(@Valid @RequestBody ShippingMethodRQ request) {
    shippingMehtodService.createShippingMethod(request);
    return response(HttpStatus.CREATED);
}
    @PutMapping("/{id}")
    public ResponseEntity<StructureRS>updateShippingMethod(@Valid @PathVariable Long id,@RequestBody UpdateShippingMethodRQ request){
        shippingMehtodService.updateShippingMethod(id,request);
        return response(HttpStatus.OK);
    }
    @DeleteMapping("/{id}")
    public ResponseEntity<StructureRS>deleteShippingMethod (@PathVariable Long id){
        shippingMehtodService.deleteShippingMethod(id);
        return response(HttpStatus.OK);
    }

}
