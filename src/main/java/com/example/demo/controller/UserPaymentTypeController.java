package com.example.demo.controller;

import com.example.demo.base.BaseController;
import com.example.demo.base.BaseListingRQ;
import com.example.demo.base.StructureRS;
import com.example.demo.model.request.UserPaymentRQ.UserPaymentTypeRQ;
import com.example.demo.service.UserPaymentType.UserPaymentTypeService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/v1/paymenttype")
@RequiredArgsConstructor
public class UserPaymentTypeController extends BaseController {
    private final UserPaymentTypeService paymentTypeService;
    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<StructureRS> getPaymentType(BaseListingRQ request){
        return response(paymentTypeService.getPaymentType(request));
    }
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<StructureRS> createPaymentType(@Validated @RequestBody UserPaymentTypeRQ request){
       paymentTypeService.createPaymentType(request);
        return response(HttpStatus.CREATED);
    }
    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<StructureRS> updatePaymentType(@Validated @PathVariable Long id, @RequestBody UserPaymentTypeRQ request){
        paymentTypeService.updatePaymentType(id, request);
        return response(HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<StructureRS> deletePaymentType(@Validated @PathVariable Long id){
        paymentTypeService.deletePaymentType(id);
        return response(HttpStatus.OK);
    }
}
