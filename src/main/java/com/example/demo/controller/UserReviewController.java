package com.example.demo.controller;

import com.example.demo.base.BaseController;
import com.example.demo.base.BaseListingRQ;
import com.example.demo.base.StructureRS;
import com.example.demo.model.request.UserReviewRQ.UpdateUserReviewRQ;
import com.example.demo.model.request.UserReviewRQ.UserReviewRQ;
import com.example.demo.service.UserReview.UserReviewService;
import jakarta.validation.Valid;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/products")
@RequiredArgsConstructor
public class UserReviewController extends BaseController {

    private final UserReviewService userReviewService;

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/user-review")
    public ResponseEntity<StructureRS> createUserReviewProduct(@Valid @RequestBody UserReviewRQ request){
        userReviewService.createUserreview(request);
        return response(HttpStatus.CREATED);
    }
    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/user-review")
    public ResponseEntity<StructureRS>getAllUserReview(@RequestParam(required = false, name = "ratingValue") Double rating_value ,BaseListingRQ request){
        return response(userReviewService.getAllUserReview(rating_value,request));
    }
    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/user-review-update/{id}")
    public ResponseEntity<StructureRS>updateUserReview(@PathVariable Long id,@Valid @RequestBody UpdateUserReviewRQ request){
        userReviewService.updateUserReview(id,request);
        return response(HttpStatus.OK);
    }
    @ResponseStatus(HttpStatus.OK)
    @DeleteMapping("/user-review-delete/{id}")
    public ResponseEntity<StructureRS>deleteUserReview(@PathVariable Long id){
        userReviewService.deleteUserReview(id);
        return response(HttpStatus.OK);
    }
}
