package com.example.demo.controller;

import com.example.demo.base.BaseController;
import com.example.demo.base.BaseListingRQ;
import com.example.demo.base.StructureRS;
import com.example.demo.db.status.Provider;
import com.example.demo.model.request.UserPaymentRQ.UserPaymentRQ;
import com.example.demo.security.UserPrincipal;
import com.example.demo.service.UserPayment.UserPaymentService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/v1/payment")
@RequiredArgsConstructor
public class UserPaymentController extends BaseController {
    private final UserPaymentService userPaymentService;
    @ResponseStatus(HttpStatus.OK)
    @GetMapping
    public ResponseEntity <StructureRS> getPayment(BaseListingRQ request, @RequestParam(required = false, defaultValue = "ALL") Provider provider){
        return response(userPaymentService.getPayment(request, provider));
    }
    @GetMapping("/paymentuser")
    public ResponseEntity <StructureRS> getpayment(JwtAuthenticationToken jwtAuthenticationToken){
        return response(userPaymentService.getPaymentToken(UserPrincipal.build(jwtAuthenticationToken)));
    }

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping
    public ResponseEntity<StructureRS> Create(@Validated @RequestBody UserPaymentRQ request,JwtAuthenticationToken jwtAuthenticationToken) {
        userPaymentService.Create(request,UserPrincipal.build(jwtAuthenticationToken));
        return response( HttpStatus.CREATED);
    }
    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/{id}")
    public ResponseEntity<StructureRS> Update(@Validated @PathVariable Long id, @RequestBody UserPaymentRQ request){
        userPaymentService.Update(id,request);
        return response(HttpStatus.OK);
    }
    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/defaultStatus/{id}")
    public ResponseEntity<StructureRS>updateStatusDefault(@PathVariable Long id){
       userPaymentService.updateStatusDefault(id);
        return response(HttpStatus.OK);
    }
    @ResponseStatus(HttpStatus.OK)
    @DeleteMapping("/{id}")
    public ResponseEntity<StructureRS> Delete(@Validated @PathVariable Long id){
        userPaymentService.Delete(id);
        return response(HttpStatus.OK);
    }
}
